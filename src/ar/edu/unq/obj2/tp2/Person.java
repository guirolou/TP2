package ar.edu.unq.obj2.tp2;

import java.time.LocalDate;
import java.time.Period;

public class Person {
	private String name;
	private String surname;
	private LocalDate birth;
	
	public Person(String name, String surname, LocalDate birth) {
		this.name = name;
		this.surname = surname;
		this.birth = birth;
	}
	
	public Person(String name, String surname, int age) {
		this.name = name;
		this.surname = surname;
		this.birth = LocalDate.now().minusYears(age);
	}
	public int getAge() {
		return Period.between(birth, LocalDate.now()).getYears();
	}
	
	public LocalDate getBirth() {
		return birth;
	}

	public boolean isYoungerThan(Person otherPerson) {
		return birth.isAfter(otherPerson.getBirth());
	}

}
