package ar.edu.unq.obj2.tp2;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PointTestCase {
	Point point, pointA, pointB;
	
	@Before
    public void setUp() throws Exception {
        point = new Point();
        pointA = new Point(5.0,3.0);
        pointB = new Point(-2.0,0.0);
    }
	
	@Test
	public void testPointZeroZero() {
		assertEquals(0.0, point.getX(), 0.0);
		assertEquals(0.0, point.getY(), 0.0);
	}
	
	@Test
	public void testPointCoords() {
		assertEquals(5.0, pointA.getX(), 0.0);
		assertEquals(3.0, pointA.getY(), 0.0);
	}
	
	@Test
	public void testMovePoint() {
		pointA.move(pointB);
		assertEquals(-2.0, pointA.getX(), 0.0);
		assertEquals(0.0, pointA.getY(), 0.0);
	}
	
	@Test
	public void testSumPoint() {
		Point pointSum = pointA.sum(pointB);
		assertEquals(3.0, pointSum.getX(), 0.0);
		assertEquals(3.0, pointSum.getY(), 0.0);
	}
}
