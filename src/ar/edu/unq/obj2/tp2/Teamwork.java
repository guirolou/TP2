package ar.edu.unq.obj2.tp2;

import java.util.ArrayList;

public class Teamwork {
	public Teamwork(String name) {
		this.name = name;
		this.people = new ArrayList<Person>();
	}

	private String name;
	private ArrayList<Person> people;

	public void add(Person person) {
		people.add(person);
		
	}

	public double average() {
		return this.ageSum() / people.size();
	}
	
	private int ageSum(){
		return people.stream().map(p -> p.getAge()).reduce(0, (x, y) -> x + y);
	}

	public String getName() {
		return name;
	}
}
