package ar.edu.unq.obj2.tp2;

public class Rectangle  extends Exception {

	private Point begin;
	private double width, height;
	
	public Rectangle(Point begin, double width, double height) throws Exception{
		this.begin = begin;
		this.width = width;
		this.height = height;
		this.validateConstructor();
		this.validateNotSquare();
	}
	
	private void validateNotSquare() throws Exception{
		if(width == height){
			throw new Exception("width and height must be positives");
		}
	}

	private void validateConstructor() throws Exception{
		if(width <= 0.0 || height <= 0.0){
			throw new Exception("width and height must be positives");
		}

	}

	public double area() {
		return width * height;
	}

	public double perimeter() {
		// TODO Auto-generated method stub
		return (width + height) * 2;
	}

	public boolean isWide() {
		return width > height;
	}
}
