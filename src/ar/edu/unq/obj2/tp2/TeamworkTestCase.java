package ar.edu.unq.obj2.tp2;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class TeamworkTestCase {
	private Teamwork teamwork;
	
	@Before
    public void setUp() throws Exception {
		teamwork = new Teamwork("pepe");
		teamwork.add(new Person("Juan", "Perez", 35));
		teamwork.add(new Person("Juan", "Saraza", 15));
		teamwork.add(new Person("Juan", "Gomez", 25));
		teamwork.add(new Person("Mario", "Gomez", 5));
		teamwork.add(new Person("Maria", "Gomez", 45));
	}    
	
	@Test
	public void TestAvgAgeIs25(){
		assertEquals(25.0, teamwork.average(), 0.0);
	}
	
	@Test
	public void TestName(){
		assertEquals("pepe", teamwork.getName());
	}
}
