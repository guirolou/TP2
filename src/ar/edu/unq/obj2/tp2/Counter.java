package ar.edu.unq.obj2.tp2;

import java.util.ArrayList;

public class Counter {

	private ArrayList<Integer> list;
	
	
	public Counter() {
		list = new ArrayList<Integer>();
	}
	
	public void addNumber(int i) {
		list.add(i);
	}

	public int getEvenOcurrences() {
		return (int)list.stream().filter(i -> i % 2 == 0).count();
	}
	
	/**
	 * @return count of even numbers
	 */
	public int getOddOcurrences() {
		return list.size() - this.getEvenOcurrences();
	}

	public int getMultiplesOfOcurrences(int i) {
		return (int)list.stream().filter(n -> n % i == 0).count();
	}
	
}
