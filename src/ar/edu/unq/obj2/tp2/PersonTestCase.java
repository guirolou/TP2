package ar.edu.unq.obj2.tp2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PersonTestCase {
	private Person person; 
	private Person otherPerson;
	
	@Before
    public void setUp() throws Exception {
		person = new Person("Brian", "Craig", LocalDate.of(1994, 10, 9));
		otherPerson = new Person("Guillermo", "Romero", 36);
	}    
	
	@Test
	public void TestAgeIs22YearsOld(){
		assertEquals(22, person.getAge());
	}
	
	@Test
	public void TestPersonIsYoungerThanOtherPerson(){
		assertTrue(person.isYoungerThan(otherPerson));
	}
		
}
