package ar.edu.unq.obj2.tp2;

public class Point {
	
	private double x;
	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	private double y;
	

	public Point() {
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}


	public void move(Point point) {
		x = point.getX();
		y = point.getY();
	}


	public Point sum(Point point) {
		return new Point(x + point.getX(), y + point.getY());
	}
}
