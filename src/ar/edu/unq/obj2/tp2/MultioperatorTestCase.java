package ar.edu.unq.obj2.tp2;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MultioperatorTestCase {
	private Multioperator multi;	
	
	@Before
    public void setUp() throws Exception {
        
        multi = new Multioperator();
        
        //Se agregan los numeros. Un solo par y nueve impares
        multi.addNumber(5);
        multi.addNumber(3);
        multi.addNumber(7);
        multi.addNumber(9);
        multi.addNumber(4);
    }
	
	@Test
	public void TestMultiply(){
		assertEquals(5*3*7*9*4, multi.multiply());
	}
	
	@Test
	public void TestSum(){
		assertEquals(5+3+7+9+4, multi.sum());
	}
	
	@Test
	public void TestSubstract(){
		assertEquals(0-5-3-7-9-4, multi.substract());
	}
}
