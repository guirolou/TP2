package ar.edu.unq.obj2.tp2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RectangleTestCase extends Exception{
	
	private Rectangle rectangle;

	@Before
    public void setUp() throws Exception {
        rectangle = new Rectangle(new Point(3.0, 2.0),3.0, 2.0 );
    }
	
	@Test(expected=Exception.class)
	public void testBadConstructor() throws Exception{
		new Rectangle(new Point(3.0, 2.0),3.0, -2.0 );
	}
	
	@Test(expected=Exception.class)
	public void testNotASquare() throws Exception{
		new Rectangle(new Point(3.0, 2.0),3.0, 3.0);
	}
	
	@Test
	public void testArea() {
		assertEquals(6.0, rectangle.area(), 0.0);
	}
	
	@Test
	public void testPerimeter() {
		assertEquals(10.0, rectangle.perimeter(), 0.0);
	}
	
	@Test
	public void testWide() { //Horizontal
		assertTrue(rectangle.isWide());
	}
}
