package ar.edu.unq.obj2.tp2;

import java.util.ArrayList;

public class Multioperator {

	private ArrayList<Integer> list = new ArrayList<Integer>();

	public void addNumber(int i) {
		list.add(i);
	}

	public int multiply() {
		return list.stream().reduce(1, (x, y) -> x * y);
	}
	
	public int sum() {
		return list.stream().reduce(0, (x, y) -> x + y);
	}
	
	public int substract() {
		return list.stream().reduce(0, (x, y) -> x - y);
	}
	

}
